﻿using System.Collections.Generic;
using DataRepository.Mappers;
using Xunit;

namespace DataRepositoryTests.Mappers
{
    public class CameraDtoMapperTests
    {
        [Fact]
        public void CameraDtoMapper_WithValidStringArray_ShouldReturnCamera()
        {
            var sut = new CameraDtoMapper();

            var stringList = new List<string> {"UTR-CM-501 Neude rijbaan voor Postkantoor", "Lat", "Long"};
            var camera = sut.MapToCameraDto(stringList.ToArray());
            
            Assert.Equal("UTR-CM-501", camera.Code);
            Assert.Equal("Neude rijbaan voor Postkantoor", camera.Name);
            Assert.Equal("Lat", camera.Latitude);
            Assert.Equal("Long", camera.Longitude);
        }
        
        [Fact]
        public void CameraDtoMapper_WithValidStringArrayWithDash_ShouldReturnCamera()
        {
            var sut = new CameraDtoMapper();
            var stringList = new List<string> {"UTR-CM-743-Boelesteinlaan - Queeckhovenplein", "Lat", "Long"};

            var camera = sut.MapToCameraDto(stringList.ToArray());
            
            Assert.Equal("UTR-CM-743", camera.Code);
            Assert.Equal("Boelesteinlaan - Queeckhovenplein", camera.Name);
            Assert.Equal("Lat", camera.Latitude);
            Assert.Equal("Long", camera.Longitude);
        }
    }
}