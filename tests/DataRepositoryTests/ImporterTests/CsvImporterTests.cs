using System;
using System.IO;
using System.Linq;
using DataRepository.Importer;
using DataRepository.Mappers;
using Xunit;

namespace DataRepositoryTests
{
    public class CsvImporterTests
    {
        private CameraDtoMapper CreateConcreteCameraDtoMapper()
        {
            return new CameraDtoMapper();
        }

        private const string InfiCsvPath =
            @"Data/cameras-defb.csv";

        [Fact]
        public void CsvImporterImport_WithInvalidPath_ShouldThowException()
        {
            var sut = new CameraCsvImporter(CreateConcreteCameraDtoMapper());
            Assert.Throws<ArgumentException>(() => sut.Import(string.Empty));
        }
        
        [Fact]
        public void CsvImporterImport_WithInvalidPath_ShouldThowException2()
        {
            var sut = new CameraCsvImporter(CreateConcreteCameraDtoMapper());
            Assert.Throws<FileNotFoundException>(() => sut.Import("wrong path"));
        }
        
        [Fact]
        public void CsvImporterImport_WithValidCsv_ShouldReturnCamera()
        {
            var sut = new CameraCsvImporter(CreateConcreteCameraDtoMapper());
            var cameras =  sut.Import(InfiCsvPath);
           
            Assert.Equal("UTR-CM-501", cameras.FirstOrDefault().Code);
            Assert.Equal("Neude rijbaan voor Postkantoor", cameras.FirstOrDefault().Name);
        }
        
        [Fact]
        public void CsvImporterImport_WithValidCsv_ShouldReturnRightAmountOfItem()
        {
            var sut = new CameraCsvImporter(CreateConcreteCameraDtoMapper());
            var cameras =  sut.Import(InfiCsvPath);
           
            Assert.Equal(88, cameras.Count);
        }
        
        [Fact]
        public void CsvImporterImport_WithValidCsv_ShouldNotReturnEmptyCamera()
        {
            var sut = new CameraCsvImporter(CreateConcreteCameraDtoMapper());
            var cameras =  sut.ImportFromDataRepository(@"Data/cameras-defb_WithEmptyLines.csv");

            foreach (var cameraDto in cameras)
            {
                Assert.NotEmpty(cameraDto.Name);
                Assert.NotEmpty(cameraDto.Latitude);
                Assert.NotEmpty(cameraDto.Longitude);
            }
            
            Assert.Equal(6, cameras.Count);
        }
    }
}