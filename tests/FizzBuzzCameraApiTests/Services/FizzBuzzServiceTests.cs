﻿using FizzBuzzCameraApi.Models;
using FizzBuzzCameraApi.Services;
using FizzBuzzCameraApi.ViewModels;
using Xunit;

namespace FizzBuzzCameraApiTests.Services
{
    public class FizzBuzzServiceTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(4)]
        [InlineData(7)]
        public void CalculateFizzBuzz_WithNumberNotDivBy3And5_ShouldReturnNotDivBy5And3(int input)
        {
            var sut = new FizzBuzzService();
            var camera = new Camera {Number = input};

            FizzBuzzCamera apiResponse = sut.CalculateFizzBuzz(camera);

            Assert.Equal(FizzBuzzOption.NotDivBy5And3, apiResponse.FizzBuzzOption);
        }
        
        [Theory]
        [InlineData(3)]
        [InlineData(6)]
        [InlineData(9)]
        [InlineData(12)]
        public void CalculateFizzBuzz_WithNumberDivBy3_ShouldReturnDiv3(int input)
        {
            var sut = new FizzBuzzService();
            var camera = new Camera {Number = input};

            FizzBuzzCamera apiResponse = sut.CalculateFizzBuzz(camera);

            Assert.Equal(FizzBuzzOption.Div3, apiResponse.FizzBuzzOption);
        }
     
        [Theory]
        [InlineData(5)]
        [InlineData(10)]
        [InlineData(20)]
        public void CalculateFizzBuzz_WithNumberDiv5_ShouldReturnDiv5(int input)
        {
            var sut = new FizzBuzzService();
            var camera = new Camera {Number = input};

            FizzBuzzCamera apiResponse = sut.CalculateFizzBuzz(camera);

            Assert.Equal(FizzBuzzOption.Div5, apiResponse.FizzBuzzOption);
        }
        
        [Theory]
        [InlineData(15)]
        [InlineData(30)]
        public void CalculateFizzBuzz_WithNumberDiv5AndDiv3_ShouldReturnDiv5AndDiv3(int input)
        {
            var sut = new FizzBuzzService();
            var camera = new Camera {Number = input};

            FizzBuzzCamera apiResponse = sut.CalculateFizzBuzz(camera);

            Assert.Equal(FizzBuzzOption.Div5And3, apiResponse.FizzBuzzOption);
        }
        
    }
}