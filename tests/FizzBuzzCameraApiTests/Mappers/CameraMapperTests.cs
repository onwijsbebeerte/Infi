using System;
using DataRepository.DtoModels;
using FizzBuzzCameraApi.Mappers;
using FizzBuzzCameraApi.Models;
using Xunit;

namespace FizzBuzzCameraApiTests
{
    public class CameraMapperTests
    {
        [Fact]
        public void CameraMapper_WithValidCameraDtoModel_ShouldMapToCamera()
        {
            var cameraDto = new CameraDto("UTR-CM-557", "Stationsplein", "52.04", "5.11");
            var expected = new Camera
            {
                Code = "UTR-CM-557",
                Latitude = "52.04",
                Number = 557,
                Name = "Stationsplein",
                Longitude = "5.11"
            };

            var sut = new CameraMapper();
            var result =  sut.MapToCamera(cameraDto);
            
            Assert.Equal(expected.Code, result.Code);
            Assert.Equal(expected.Number, result.Number);
            Assert.Equal(expected.Name, result.Name);
            Assert.Equal(expected.Latitude, result.Latitude);
            Assert.Equal(expected.Longitude, result.Longitude);
        }
    }
}