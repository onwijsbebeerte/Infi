# Infi opdracht

### Install instructions

cd naar src/ en run daar `dotnet build` dit zou alle projecten moeten compilen.

De webserver zit in `/src/FizzBuzzCameraApi`

deze kan je starter door `dotnet run` uit te voeren, je krijgt dan een ip en een port te zien in de terminal. Bijvoorbeeld localhost:5000, de index.html is te vinden op localhost:5000/index.html

De Cli tool zit in `/src/CameraSearchCli/` deze kan je runnen door vanuit deze directory `dotnet run --name 'rijbaan voor'` uit te voeren.

## Korte toelichting 

### Web Api

in index.html worden 2 Ajax calls, 1 om de data van de columns te halen, en 1 om de markers te halen voor google maps. (zie [postman collection](/FizzBuzzCameraApi.postman_collection.json)  voor details).

De web api doet het importeren van de CSV (via een externe Data Repository class library die ik hergebruik voor de CLI tool).
Daarnaast maakt de Web api server van de domain models viewmodels en is het verantwoordelijk voor het fizzbuzz gedeelte.

in `/src/FizzBuzzCameraApi/appsettings.json` is nog wat extra configuratie te vinden, bijv de path naar de CSV en wat caching mogelijkheden.

### Cli tool

De Cli is niet heel veel bijzonders te melden, momenteel kan je alleen zoeken op naam, en is er een volledige run nodig van de console applicatie.

Verder is er gebruik gemaakt van een handmatige DI wanneer de applicatie runt.

### tot slot

Volgens mij zitten de opdrachten wel redelijk in elkaar, dingen die nog verbeterd zouden kunnen worden is wat defensiever programeren (er zitten vrij weinig null checks in de applicatie) en er mist momenteel logging.

Daarnaast heb ik mijn twijfels over hoe gezond het is dat de Google's Apikey gewoon in de frontend staat. Dat leek mij voor deze opdracht niet heel belangrijk, maar wel iets om rekening mee te houden als bijvoorbeeld in productie draait.

[Architectuur plaat] (/InfiOpdracht.png)
