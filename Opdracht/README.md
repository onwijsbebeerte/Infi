# Infi interview

De opdracht bestaat uit drie onderdelen waarin je gebruik maakt van de dataset die je treft in [data/cameras-defb.csv](data/cameras-defb.csv). Het gebruik van een PHP-framework en externe libraries - met uitzondering van jQuery en de Google Maps API - is niet toegestaan.

## Maak een Ajax-call met jQuery
In dit eerste onderdeel van de test haal je de data uit de gegeven dataset op middels een Ajax-request in jQuery en toon je die data verspreid over vier kolommen in de gegeven [index.html](code/index.html). De spreiding van de data gebeurt volgens de onderstaande regels en op basis van het _number_ van de camera:
1. Als het _number_ van de camera deelbaar is door 3, dan belandt hij in de eerste kolom
2. Als het _number_ van de camera deelbaar is door 5, dan belandt hij in de tweede kolom
3. Als het _number_ van de camera deelbaar is door zowel 3 als door 5, dan belandt hij in de derde kolom
4. Als het _number_ van de camera niet deelbaar is door 3 en niet deelbaar is door 5, dan belandt hij in de laatste kolom

**Format van cameranaam**  
**Name:** UTR-CM-501 Neude rijbaan voor Postkantoor  
**Id:** UTR-CM-501  
**Number:** 501

## Toon cameras als markers in Google Maps
Toon _alle_ camera's in de `div` met id _map_ in de gegeven [index.html](code/index.html). Uitleg over de werking, het aanmaken van een API-key en een code-voorbeeld vind je [hier](https://developers.google.com/maps/documentation/javascript/examples/marker-simple).

## Zoeken via CLI
Maak een script dat de gebruiker in staat stelt via de CLI te zoeken op een deel van een camera _name_, voorbeeld:
```
command:
php search.php --name Neude                                                          

output:
501 | UTR-CM-501 Neude rijbaan voor Postkantoor | 52.093421 | 5.118278
503 | UTR-CM-503 Neude plein | 52.093448 | 5.118536
504 | UTR-CM-504 Neude / Schoutenstraat | 52.092995 | 5.119088
505 | UTR-CM-505 Neude / Drakenburgstraat / Vinkenurgstraat | 52.092843 | 5.118351
506 | UTR-CM-506 Vinkenburgstraat / Neude | 52.092378 | 5.117902
507 | UTR-CM-507 Vinkenburgstraat richting Neude | 52.092234 | 5.117766
```
