﻿using System.Collections.Generic;
using FizzBuzzCameraApi.Models;

namespace FizzBuzzCameraApi.ViewModels
{
    public class GoogleMapView
    {
        public Marker Center { get; set; }
        public List<Marker> Markers { get; set; }
        public int Zoom { get; set; }
    }
}