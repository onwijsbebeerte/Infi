﻿using FizzBuzzCameraApi.Models;

namespace FizzBuzzCameraApi.ViewModels
{
    public class FizzBuzzCamera
    {
        public Camera Camera { get; set; }
        
        public FizzBuzzOption FizzBuzzOption { get; set; }
      
        public string FizzBuzzOptionDisplayString { get; set; }
    }
}