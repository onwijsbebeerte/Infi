﻿namespace FizzBuzzCameraApi.Constants
{
    public static class ConfigurationKeys
    {
        public const string InfiCsvPath = "InfiCsvPath";
        public const string retrieveCamerasFromCache = "retrieveCamerasFromCache";
        public const string CachingInMinuites = "CachingInMinuites";
    }
}