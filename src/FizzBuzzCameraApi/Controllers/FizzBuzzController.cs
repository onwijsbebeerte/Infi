﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FizzBuzzCameraApi.Models;
using FizzBuzzCameraApi.Services;
using FizzBuzzCameraApi.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FizzBuzzCameraApi.Controllers
{
    [DisableCors]
    [Route("api/[controller]")]
    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzCameraFactory _fizzBuzzCameraFactory;

        public FizzBuzzController(IFizzBuzzCameraFactory fizzBuzzCameraFactory)
        {
            _fizzBuzzCameraFactory = fizzBuzzCameraFactory;
        }
        // GET api/FizzBuzz
        
        [HttpGet]
        public List<FizzBuzzCamera> Get()
        {
           var result = _fizzBuzzCameraFactory.CreateFizzBuzzResponse();
            
           return result;
        }
    }
}
