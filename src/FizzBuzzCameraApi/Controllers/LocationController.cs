﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FizzBuzzCameraApi.Models;
using FizzBuzzCameraApi.Services;
using FizzBuzzCameraApi.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace FizzBuzzCameraApi.Controllers
{
    [DisableCors]
    [Route("api/[controller]")]
    public class LocationController : Controller
    {
        private readonly IGoogleMapsFactory _googleMapsFactory;

        public LocationController(IGoogleMapsFactory googleMapsFactory)
        {
            _googleMapsFactory = googleMapsFactory;
        }
        
        [HttpPost]
        public IActionResult Get([FromBody] GoogleMapRequest request)
        {
            if (ModelState.IsValid)
            {
                var result = _googleMapsFactory.CreateGoogleMapsResponse(request);

                return Ok(result);
            }

            return BadRequest();
        }
    }
}
