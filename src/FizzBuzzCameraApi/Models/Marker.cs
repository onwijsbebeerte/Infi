﻿namespace FizzBuzzCameraApi.Models
{
    public class Marker
    {
        public string Lat { get; set; }
        public string Lng { get; set; }
        public string Title { get; set; }
    }
}