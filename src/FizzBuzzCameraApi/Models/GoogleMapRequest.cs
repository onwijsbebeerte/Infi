﻿namespace FizzBuzzCameraApi.Models
{
    public class GoogleMapRequest
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Zoom { get; set; }
    }
}