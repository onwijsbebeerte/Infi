﻿namespace FizzBuzzCameraApi.Models
{
    public class Camera
    {
        public string Name { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public string Code { get; set; }
        
        public int Number { get; set; }
    }
}