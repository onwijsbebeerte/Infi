﻿namespace FizzBuzzCameraApi.Models
{
    public enum FizzBuzzOption
    {
        Div3,
        Div5,
        Div5And3,
        NotDivBy5And3
    }
}