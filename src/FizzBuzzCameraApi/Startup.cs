﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataRepository.Importer;
using DataRepository.Mappers;
using DataRepository.Repositories;
using FizzBuzzCameraApi.Mappers;
using FizzBuzzCameraApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FizzBuzzCameraApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddMemoryCache();
            services.AddTransient<ICameraService, CameraService>();
            services.AddTransient<ICameraRepository, CameraRepository>();
            services.AddTransient<ICameraCsvImporter, CameraCsvImporter>();
            services.AddTransient<ICameraDtoMapper, CameraDtoMapper>();
            services.AddTransient<ICameraMapper, CameraMapper>();
            services.AddTransient<IFizzBuzzCameraFactory, FizzBuzzCameraFactory>();
            services.AddTransient<IFizzBuzzService, FizzBuzzService>();
            services.AddTransient<IGoogleMapMarkerMapper, GoogleMapMarkerMapper>();
            services.AddTransient<IGoogleMapsFactory, GoogleMapsFactory>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors(builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}
