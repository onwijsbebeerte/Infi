﻿using FizzBuzzCameraApi.Models;

namespace FizzBuzzCameraApi.Mappers
{
    public interface IGoogleMapMarkerMapper
    {
        Marker Map(Camera cameras);
    }
    
    public class GoogleMapMarkerMapper : IGoogleMapMarkerMapper
    {
        public Marker Map(Camera camera)
        {
            var marker = new Marker();

            marker.Lat = camera.Latitude;
            marker.Lng = camera.Longitude;
            marker.Title = camera.Name;

            return marker;
        }
    }
}