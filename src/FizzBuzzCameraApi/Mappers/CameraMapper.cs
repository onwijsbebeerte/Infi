﻿using System.Text.RegularExpressions;
using DataRepository.DtoModels;
using FizzBuzzCameraApi.Models;

namespace FizzBuzzCameraApi.Mappers
{
    public class CameraMapper : ICameraMapper
    {
        public Camera MapToCamera(CameraDto cameraDto)
        {
            var model = new Camera
            {
                Code = cameraDto.Code,
                Latitude = cameraDto.Latitude,
                Longitude = cameraDto.Longitude,
                Name = cameraDto.Name
            };
            
            var numberString = Regex.Match(cameraDto.Code, @"\d+").Value;

            if (int.TryParse(numberString, out var result))
            {
                model.Number = result;
            }
            
            return model;
        }
    }

    public interface ICameraMapper
    {
        Camera MapToCamera(CameraDto cameraDto);
    }
}