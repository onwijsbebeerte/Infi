﻿using System.Collections.Generic;
using System.Linq;
using FizzBuzzCameraApi.Mappers;
using FizzBuzzCameraApi.Models;
using FizzBuzzCameraApi.ViewModels;

namespace FizzBuzzCameraApi.Services
{
    public interface IGoogleMapsFactory
    {
        GoogleMapView CreateGoogleMapsResponse(GoogleMapRequest request);
    }

    public class GoogleMapsFactory : IGoogleMapsFactory
    {
        private readonly ICameraService _cameraService;
        private readonly IGoogleMapMarkerMapper _googleMapMarkerMapper;

        public GoogleMapsFactory(ICameraService cameraService, IGoogleMapMarkerMapper googleMapMarkerMapper)
        {
            _cameraService = cameraService;
            _googleMapMarkerMapper = googleMapMarkerMapper;
        }

        public GoogleMapView CreateGoogleMapsResponse(GoogleMapRequest request)
        {
            var cameras = _cameraService.GetAllCamers();

            var markers = cameras.Select(x => _googleMapMarkerMapper.Map(x)).ToList();
            var viewmodel = new GoogleMapView
            {
                Markers = markers,
                Center = new Marker {Lat = request.Latitude.ToString(), Lng = request.Longitude.ToString()},
                Zoom = request.Zoom
            };

            return viewmodel;
        }
    }
}