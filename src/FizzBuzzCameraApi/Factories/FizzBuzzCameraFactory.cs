﻿using System.Collections.Generic;
using System.Linq;
using FizzBuzzCameraApi.ViewModels;

namespace FizzBuzzCameraApi.Services
{
    public interface IFizzBuzzCameraFactory
    {
        List<FizzBuzzCamera> CreateFizzBuzzResponse();
    }
    
    public class FizzBuzzCameraFactory : IFizzBuzzCameraFactory
    {
        private readonly ICameraService _cameraService;
        private readonly IFizzBuzzService _fizzBuzzService;

        public FizzBuzzCameraFactory(ICameraService cameraService, IFizzBuzzService fizzBuzzService)
        {
            _cameraService = cameraService;
            _fizzBuzzService = fizzBuzzService;
        }
        public List<FizzBuzzCamera> CreateFizzBuzzResponse()
        {
            return _cameraService.GetAllCamers().Select(x => _fizzBuzzService.CalculateFizzBuzz(x)).ToList();
        }
    }
}