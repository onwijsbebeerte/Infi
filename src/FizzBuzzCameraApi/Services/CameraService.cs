﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataRepository.DtoModels;
using DataRepository.Repositories;
using FizzBuzzCameraApi.Constants;
using FizzBuzzCameraApi.Mappers;
using FizzBuzzCameraApi.Models;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;

namespace FizzBuzzCameraApi.Services
{
    public interface ICameraService
    {
        List<Camera> GetAllCamers();
    }

    public class CameraService : ICameraService
    {
        private readonly ICameraRepository _cameraRepository;
        private readonly ICameraMapper _cameraMapper;
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _memoryCache;

        public CameraService(ICameraRepository cameraRepository, ICameraMapper cameraMapper,
            IConfiguration configuration, IMemoryCache memoryCache)
        {
            this._cameraRepository = cameraRepository;
            this._cameraMapper = cameraMapper;
            this._configuration = configuration;
            this._memoryCache = memoryCache;
        }

        public List<Camera> GetAllCamers()
        {      
            var cameraDtos = getCameraDtos();

            var cameras = cameraDtos.Select(x => _cameraMapper.MapToCamera(x)).ToList();

            return cameras;
        }

        private List<CameraDto> getCameraDtos()
        {
            var path = _configuration.GetValue<string>(ConfigurationKeys.InfiCsvPath);

            if (string.IsNullOrEmpty(path))
            {
                throw new ArgumentException("Path not specified.");
            }
            
            bool retrieveFromCache = _configuration.GetValue<bool>(ConfigurationKeys.retrieveCamerasFromCache);
            int cachingInMinuites = _configuration.GetValue<int>(ConfigurationKeys.CachingInMinuites);
            
            var cameraDtos = new List<CameraDto>();
            
            if (retrieveFromCache)
            {
                cameraDtos  = _memoryCache.GetOrCreate(CacheKeys.CameraDtos,
                    cacheEntry =>
                    {
                        cacheEntry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(cachingInMinuites);
                        return _cameraRepository.GetCamerasFromDataRepository(path);
                    });
            }
            else
            {
                _memoryCache.Remove(CacheKeys.CameraDtos);
                cameraDtos = _cameraRepository.GetCamerasFromDataRepository(path);
            }

            return cameraDtos;
        }
    }
}