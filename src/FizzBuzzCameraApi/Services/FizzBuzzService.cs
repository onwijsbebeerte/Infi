﻿using FizzBuzzCameraApi.Models;
using FizzBuzzCameraApi.ViewModels;

namespace FizzBuzzCameraApi.Services
{
    public class FizzBuzzService : IFizzBuzzService
    {
        public FizzBuzzCamera CalculateFizzBuzz(Camera camera)
        {
            var fizzBuzzOption = FizzBuzz(camera.Number);
            var result = new FizzBuzzCamera
            {
                FizzBuzzOption = fizzBuzzOption,
                FizzBuzzOptionDisplayString = fizzBuzzOption.ToString(),
                Camera = camera
            };

            return result;
        }

        private FizzBuzzOption FizzBuzz(int input)
        {
            var result = FizzBuzzOption.NotDivBy5And3;

            if(input == 0) return result;

            var divBy3 = input % 3 == 0;
            var divBy5 = input % 5 == 0;
            
            if (divBy3 && divBy5)
            {
                result = FizzBuzzOption.Div5And3;
            }
            else if (divBy3)
            {
                result = FizzBuzzOption.Div3;
            }
            else if (divBy5)
            {
                result = FizzBuzzOption.Div5;
            }

            return result;
        }
    }

    public interface IFizzBuzzService
    {
        FizzBuzzCamera CalculateFizzBuzz(Camera camera);
    }
}