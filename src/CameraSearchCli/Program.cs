﻿using System;
using System.Linq;
using CameraSearchCli.Mappers;
using DataRepository.Importer;
using DataRepository.Mappers;
using DataRepository.Repositories;

namespace CameraSearchCli
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length != 2 || args[0].ToUpper() != "--NAME")
            {
                Console.WriteLine(args[0]);
                Console.WriteLine("invalid search query, please prefix search with --name");
                Console.WriteLine("Example --name 'Neude'");
            }
            else
            {
                var searchService = CreateSearchService();
                var result = searchService.SearchByName(args[1]);
               
                Console.WriteLine("Output:");

                if (result != null && result.Any())
                {
                    Console.WriteLine($"Found: {result.Count} result(s)");

                    foreach (var searchResult in result)
                    {
                        Console.WriteLine(searchResult.DisplayString());
                    }
                }
                else
                {
                    Console.WriteLine("No results");
                }
            }
        }

        private static SearchService CreateSearchService()
        {
            return new SearchService(new CameraService(new CameraRepository(new CameraCsvImporter(new CameraDtoMapper()))), new CameraSearchResultMapper(), @"Data/cameras-defb.csv");
        }
    }
}