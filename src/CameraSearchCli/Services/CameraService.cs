﻿using System;
using System.Collections.Generic;
using DataRepository.DtoModels;
using DataRepository.Repositories;

namespace CameraSearchCli
{
    public interface ICameraService
    {
        List<CameraDto> GetCameras(string csvPath);
    }
    
    public class CameraService : ICameraService
    {
        private readonly ICameraRepository _cameraRepository;

        public CameraService(ICameraRepository cameraRepository)
        {
            _cameraRepository = cameraRepository;
        }
        public List<CameraDto> GetCameras(string csvPath)
        {
            if (string.IsNullOrEmpty(csvPath))
            {
                throw new ArgumentException("Path not specified.");
            }
            
            var cameraDtos = _cameraRepository.GetCamerasFromDataRepository(csvPath);
           
            Console.WriteLine($"searching in the database of {cameraDtos.Count} cameras");
           
            return cameraDtos;
        }
    }
}