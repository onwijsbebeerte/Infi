﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using CameraSearchCli.Mappers;
using CameraSearchCli.Models;

namespace CameraSearchCli
{
    public interface ISearchService
    {
        List<CameraSearchResult> SearchByName(string searchQuery);
    }
    
    public class SearchService : ISearchService
    {
        private readonly ICameraService _cameraService;
        private readonly ICameraSearchResultMapper _searchResultMapper;
        private readonly string _csvPath;

        public SearchService(ICameraService cameraService, ICameraSearchResultMapper searchResultMapper, string csvPath)
        {
            _cameraService = cameraService;
            _searchResultMapper = searchResultMapper;
            _csvPath = csvPath;
        }

        public List<CameraSearchResult> SearchByName(string searchQuery)
        {
            Console.WriteLine($"searching for '{searchQuery}'");
            
            var searchResults = _cameraService.GetCameras(_csvPath).Where(x => x.Name.IndexOf(searchQuery, StringComparison.OrdinalIgnoreCase) >= 0);

            return searchResults.Select(x => _searchResultMapper.Map(x)).ToList();
        }
    }
}