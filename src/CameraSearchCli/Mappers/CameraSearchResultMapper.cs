﻿using System.Text.RegularExpressions;
using CameraSearchCli.Models;
using DataRepository.DtoModels;

namespace CameraSearchCli.Mappers
{
    public interface ICameraSearchResultMapper
    {
        CameraSearchResult Map(CameraDto camera);
    }
    
    public class CameraSearchResultMapper : ICameraSearchResultMapper
    {
        public CameraSearchResult Map(CameraDto camera)
        {
           var id = Regex.Match(camera.Code, @"\d+").Value;
           return new CameraSearchResult()
           {
               Id = int.Parse(id),
               Name = camera.Name,
               Latitude = camera.Latitude,
               Longitude = camera.Longitude,
               Code = camera.Code
           };
        }
    }
}