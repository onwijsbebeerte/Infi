﻿using System.Data.Common;

namespace CameraSearchCli.Models
{
    public class CameraSearchResult
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Code { get; set; }
        public string DisplayString () =>  $"{Id} | {Code} {Name} | {Latitude} | {Longitude}";
    }
}