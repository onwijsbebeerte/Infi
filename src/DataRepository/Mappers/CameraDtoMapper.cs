﻿using System.Text.RegularExpressions;
using DataRepository.DtoModels;

namespace DataRepository.Mappers
{
    public class CameraDtoMapper : ICameraDtoMapper
    {
        public CameraDto MapToCameraDto(string[] cameraCsvLine)
        {
            //Regex takes 2 groups {2} of Capitalized A-Z ending with a dash and trialing numbers
            
            var code = Regex.Match(cameraCsvLine[0], @"([A-Z]+-){2}\d+").Value;
            var name = cameraCsvLine[0].Replace(code, "").Substring(1);
            
            return new CameraDto(code, name, cameraCsvLine[1], cameraCsvLine[2]);
        }
    }

    public interface ICameraDtoMapper
    {
        CameraDto MapToCameraDto(string[] cameraCsvLine);
    }
}