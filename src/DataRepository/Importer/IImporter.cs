﻿namespace DataRepository.Importer
{
    public interface IImporter<T>
    {
        T Import(string sourcePath);
    }
}