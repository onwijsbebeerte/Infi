﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using DataRepository.DtoModels;
using DataRepository.Mappers;

namespace DataRepository.Importer
{
    public interface ICameraCsvImporter
    {
        List<CameraDto> Import(string sourcePath);
        
        List<CameraDto> ImportFromDataRepository(string partialPath);
    }
    
    public class CameraCsvImporter : IImporter<List<CameraDto>>, ICameraCsvImporter
    {
        private readonly ICameraDtoMapper _cameraDtoMapper;

        public CameraCsvImporter(ICameraDtoMapper cameraDtoMapper)
        {
            this._cameraDtoMapper = cameraDtoMapper;
        }

        public List<CameraDto> ImportFromDataRepository(string partialPath)
        {
            Console.WriteLine("partial is: " + partialPath);
            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string path = Path.Combine(assemblyFolder,partialPath);
            Console.WriteLine("calling with: " + path);
            return Import(path);
        }
        
        public List<CameraDto> Import(string sourcePath)
        {
            var cameras = new List<CameraDto>();
           
            using(var reader = new StreamReader(sourcePath))
            {
                //skip csv header
                reader.ReadLine();

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(';');

                    if (isValidCameraLine(values))
                    {
                        cameras.Add(_cameraDtoMapper.MapToCameraDto(values));
                    }
                }
            }

            return cameras;
        }

        private bool isValidCameraLine(string[] values)
        {
            //Line should have, name. lat and long normally I wouldn't prefere multiple returns, but for validation it
            //is clearer
            if (values.Length != 3)
            {
                return false;
            }

            if (string.IsNullOrEmpty(values[0]))
            {
                return false;
            }
            
            if (string.IsNullOrEmpty(values[1]))
            {
                return false;
            }
            
            if (string.IsNullOrEmpty(values[2]))
            {
                return false;
            }

            return true;
        }
    }
}