﻿namespace DataRepository.DtoModels
{
    public class CameraDto
    {
        public CameraDto(string code, string name, string latitude, string longitude)
        {
            this.Code = code;
            this.Name = name;
            this.Latitude = latitude;
            this.Longitude = longitude;
        }
        
        public string Name { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }
        
        public string Code { get; set; }
    }
}