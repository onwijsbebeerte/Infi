﻿using System.Collections.Generic;
using DataRepository.DtoModels;
using DataRepository.Importer;

namespace DataRepository.Repositories
{
    public interface ICameraRepository
    {
        List<CameraDto> GetCameras(string sourcePath);
       
        List<CameraDto> GetCamerasFromDataRepository(string sourcePath);
    }

    public class CameraRepository : ICameraRepository
    {
        private readonly ICameraCsvImporter _cameraCsvImporter;

        public CameraRepository(ICameraCsvImporter cameraCsvImporter)
        {
            this._cameraCsvImporter = cameraCsvImporter;
        }
        
        public List<CameraDto> GetCameras(string sourcePath)
        {
            return _cameraCsvImporter.Import(sourcePath);
        }

        public List<CameraDto> GetCamerasFromDataRepository(string partialPath)
        {
            return _cameraCsvImporter.ImportFromDataRepository(partialPath);
        }
    }
}