function initMap() {
    getGoogleMapsData(function (data) {
        var myLatLng = {lat: parseFloat(data.center.lat), lng: parseFloat(data.center.lng)};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: data.zoom,
            center: myLatLng
        });

        data.markers.forEach(function (marker) {
            var markerLatLong = {lat: parseFloat(marker.lat), lng: parseFloat(marker.lng)}

            new google.maps.Marker({
                position: markerLatLong,
                map: map,
                title: marker.title
            });
        })
    })
}

function getGoogleMapsData(cb) {
    var baseUrl = "http://localhost:5000/api/Location";
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            cb(myArr);
        }
    };

    var request = JSON.stringify({
        "latitude": 52.092876,
        "Longitude": 5.104480,
        "zoom": 11
    });

    xmlhttp.open("Post", baseUrl, true);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(request);
}


var fizzBuzzColumns = document.querySelectorAll("[fizzbuzz-data]");

if (fizzBuzzColumns !== null) {
    getFizzBuzzData(function (data) {
        fizzBuzzColumns.forEach(function (column) {
            var columnType = parseInt(column.getAttribute('fizzbuzz-data'));

            var arr = data.filter(function (fizzBuzzCamera) {
                return fizzBuzzCamera.fizzBuzzOption === columnType;
            });

            arr.forEach(function (value) {
                column.querySelectorAll("tbody")[0].appendChild(createHtmlColumn(value.camera));
            });
        });
    })
}

function createHtmlColumn(data) {
    var tr = document.createElement("tr")

    var number = document.createElement("td");
    var name = document.createElement("td");
    var latitude = document.createElement("td");
    var longitude = document.createElement("td");

    var numberContent = document.createTextNode(data.number);
    number.appendChild(numberContent);

    var latitudeContent = document.createTextNode(data.latitude);
    latitude.appendChild(latitudeContent);

    var longitudeContent = document.createTextNode(data.longtitude);
    longitude.appendChild(longitudeContent);

    var nameContent = document.createTextNode(data.name);
    name.appendChild(nameContent);

    tr.appendChild(number);
    tr.appendChild(name);
    tr.appendChild(latitude);
    tr.appendChild(longitude);

    return tr;
}

function getFizzBuzzData(cb) {
    var baseUrl = "http://localhost:5000/api/FizzBuzz";
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            var myArr = JSON.parse(this.responseText);
            cb(myArr);
        }
    };

    xmlhttp.open("GET", baseUrl, true);
    xmlhttp.send();
}